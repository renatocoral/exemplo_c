#include <stdio.h>

#include "pointerToFunctions.h"

void callFunctions()
{
  void (* justPrintPointer)();
  char * (* messageToPrintPointer)();
  int (* valueToPrintPointer)();

  justPrintPointer = justPrint;
  messageToPrintPointer = messageToPrint;
  valueToPrintPointer = valueToPrint;

  justPrintPointer();
  puts(messageToPrintPointer());
  printf("ValueToPrint, returns: %d\n", valueToPrintPointer());
}

void justPrint()
{
  puts ("Yes!!! My turn :) ");
}

char * messageToPrint()
{
  return "It's me, messageToPrint! =)";
}

int valueToPrint()
{
  return 10;
}
