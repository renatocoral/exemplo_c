/**
* @file multipleArguments.h
*/

#ifndef _MULTIPLE_ARGUMENTS_H_
#define _MULTIPLE_ARGUMENTS_H_

/**
* @param _message Identification message.
* @param ... Different arguments.
* @brief Receive default message, and print the other parameters. 
*/
extern void printMultipleArguments(char * _message, ...);

extern void callMultipleTimes();

#endif
