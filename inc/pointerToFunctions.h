/**
* @file pointerToFunctions.h
*/
#ifndef _POINTER_TO_FUNCTIONS_H_
#define _POINTER_TO_FUNCTIONS_H_

/**
* @brief
*/
extern void callFunctions();

/**
* @brief
*/
extern void justPrint();

/**
* @brief
*/
extern char * messageToPrint();

/**
* @brief
*/
extern int valueToPrint();

#endif
